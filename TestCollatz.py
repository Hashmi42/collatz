#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------
import unittest
from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "200 234\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 200)
        self.assertEqual(j, 234)

    def test_read_3(self):
        s = "259 990\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 259)
        self.assertEqual(j, 990)

    # Failed Test Cases
    def test_read_4(self):
        s = "2 23\n"
        i, j = collatz_read(s)
        self.assertNotEqual(i, 1)
        self.assertNotEqual(j, 22)

    def test_read_5(self):
        s = "25 34\n"
        i, j = collatz_read(s)
        self.assertNotEqual(i, 20)
        self.assertNotEqual(j, 30)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # Failed Test Cases
    def test_eval_5(self):
        v = collatz_eval(1, 10)
        self.assertNotEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(100, 200)
        self.assertNotEqual(v, 1)

    def test_eval_7(self):
        v = collatz_eval(201, 210)
        self.assertNotEqual(v, 445)

    def test_eval_8(self):
        v = collatz_eval(900, 1000)
        self.assertNotEqual(v, 2343)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # Failed Test Cases
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertNotEqual(w.getvalue(), "10 1 20\n")

    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertNotEqual(w.getvalue(), "10 20 25\n")

    def test_print_7(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertNotEqual(w.getvalue(), "21 1 890\n")

    def test_print_8(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertNotEqual(w.getvalue(), "900 1000 173\n")

    # # -----
    # # solve
    # # -----
    #

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # Failed Test Cases
    def test_solve_2(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertNotEqual(
            w.getvalue(), "1 10 1\n100 200 1\n201 210 1\n900 1000 1\n")

    def test_solve_3(self):
        r = StringIO("456 500\n28 29\n2500 3000\n4000 5000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "456 500 142\n28 29 19\n2500 3000 217\n4000 5000 215\n")

    # Some intervals are flipped on purpose to see if the program will catch it
    def test_solve_4(self):
        r = StringIO("1 5000\n6000 5000\n5999 6000\n5000 4000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 5000 238\n6000 5000 236\n5999 6000 187\n5000 4000 215\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
