#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------

collats_cache = {1: 1, 300: 17, 350: 82, 777: 34, 900: 55, 986: 50, 1133: 63, 1500: 48, 2890: 49, 99999: 277,
                 59494: 48, 80000: 33, 33: 27, 55: 113, 66: 28, 88: 18, 99: 26, 45: 17, 144: 24, 133: 29, 222: 71
                 }


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """

    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    maxCycle = 1

    if i > j:
        i, j = j, i

    for cycles in range(i, j + 1):
        if cycles == 1:
            pass
        elif cycles in collats_cache:
            tempCycle = collats_cache[cycles]
            if tempCycle > maxCycle:
                maxCycle = tempCycle
        else:
            temp = cycles
            tempCycle = 1

            while temp > 1:
                if temp % 2 != 0:
                    temp = temp * 3 + 1
                    tempCycle += 1
                else:
                    temp = temp / 2
                    tempCycle += 1
            if cycles not in collats_cache:
                collats_cache[cycles] = tempCycle
            if tempCycle > maxCycle:
                maxCycle = tempCycle
    return maxCycle


# -------------
# collatz_print
# -------------

def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """

    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------

def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
